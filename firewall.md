## TECHNICAL PAPER ON "FIREWALL":
<p>&nbsp;</p>

## What is firewall?

*A firewall is a network security device that takes care of incoming and outgoing network traffic and makes decision to get in or block the specific inputs based on defined set of security rules.*

It is used to prevent unauthorized access to or from a private network.
<p>&nbsp;</p>

![Fig1](https://www.tunnelsup.com/images/firewall1.png)

As shown in above figure, firewall controls the exit and entry of traffics and allow only the authorized traffic networks.

<p>&nbsp;</p>

**A firewall can be a software, a hardware or both software as well as hardware.**


**Software firewall** is a program that is installed in our computer. It is used to protect a single computer or a device. It can easily differentiate between the available programs on computer. Software firewall can also be configured for checking any suspicious outgoing requests.

**Hardware firewalls** allow us to protect our entire network from the outside world with a single physical device. This device is installed between the established computer network and the internet. These firewalls are emerged into the routers that are established between the computer and internet gateway.



## Thing to remember: 

As we know that, Hardware firewall is used to connect more than one computer network to the internet. Then, don't forgot to establish the software firewall inside every computer systems. Because if some virus or worms get entry into any one of the connected computers and that computer does not have installed software firewall then, all the connected computer can suffer through that virus.

*Example:*

When we install some softwares from an untrusted website, it may or not have some installed viruses. But if there is no firewall protection on our computer system then it can cause harm to the computer system.
<p>&nbsp;</p>

## Types of Firewall :

* Packet-filtering Firewalls
* Circuit-level Gateways
* Application-level Gateways (Proxy Firewalls)
* Stateful Multi-layer Inspection (SMLI) Firewalls
* Next-generation Firewalls (NGFW)
* Threat-focused NGFW
* Network Address Translation (NAT) Firewalls
* Cloud Firewalls
* Unified Threat Management (UTM) Firewalls
<p>&nbsp;</p>


![Fig1](https://static.javatpoint.com/tutorial/firewall/images/types-of-firewall.png)
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

# References:


* [Reference1](https://www.researchgate.net/publication/2371491_An_Overview_of_Firewall_Technologies)
*  [Reference2](https://www.geeksforgeeks.org/difference-between-hardware-firewall-and-software-firewall/)
*  [Reference3](https://www.javatpoint.com/types-of-firewall)
*  [Reference4](images.google.com)
